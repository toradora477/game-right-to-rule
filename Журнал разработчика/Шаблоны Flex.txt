Свойства родительского flex-элемента – flex-контейнер
flex-direction
  row
  row-reverse
  column
  column-reverse
flex-wrap
  nowrap
  wrap
  wrap-reverse
justify-content
  flex-start
  flex-end
  center
  space-between
  space-around
align-items
  stretch
  flex-start
  flex-end
  center
  baseline
align-content
  stretch
  flex-start
  flex-end
  center
  space-between
  space-around

Свойства дочерних элементов – flex-элементы
order                   // Порядок блоков (очередность)
  (0,1,2,3)
flex-drow
  (0,1,2,3)
flex-shrink
  (0,1,2,3)
flex-basis
  auto
aling-self
  auto
  stretch
  flex-start
  flex-end
  center
  baseline
