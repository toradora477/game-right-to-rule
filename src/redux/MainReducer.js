import initState from './initState';

function reducers(state = initState, action) {
    if (action.type === 'ADD_TRACK') {
      return [
        ...state,
        action.payload
      ];
    }
    return state;
}

export default reducers;
