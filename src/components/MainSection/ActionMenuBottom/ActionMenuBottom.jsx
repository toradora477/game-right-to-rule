import React from 'react';
import GameStatistics from './GameStatistics/GameStatistics';
import PlayerStats from './PlayerStats/PlayerStats';
import a3 from './ActionMenuBottom.module.css';

const ActionMenuBottom = (props) => {
  return (
    <div className={a3.ActionMenuBottom}>
      <GameStatistics/>
      <PlayerStats/>
    </div>
  )
}

export default ActionMenuBottom;
