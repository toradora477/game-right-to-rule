import React from 'react';
import LeftParameters from './LeftParameters/LeftParameters';
import EventTasks from './EventTasks/EventTasks';
import RightParameters from './RightParameters/RightParameters';
import a2 from './ActionMenuMiddle.module.css';

const ActionMenuMiddle = (props) => {
  return (
    <div className={a2.ActionMenuMiddle}>
      {/* <LeftParameters/> */}
      <EventTasks/>
      <RightParameters/>
    </div>
  )
}

export default ActionMenuMiddle;
