import React from 'react';
import Typography from '@material-ui/core/Typography';
import ass from '../../../../assets/svg/001.svg';
import css from './StateIcon.module.scss';

const AppTitle = (props) => {
  return (
    <div className={css.AppTitle}>
      <Typography className={css.title} variant="h6" noWrap>
        Government
      </Typography>
      <img src={ass} />
    </div>
  )
}

export default AppTitle;
