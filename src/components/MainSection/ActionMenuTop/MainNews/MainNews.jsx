import React from 'react';
import Button from '@material-ui/core/Button';

import css from './MainNews.module.scss';

const MainNews = (props) => {
  return (
    <Button className={css.news}>
      Last news: 
      <span>The papers contain details about UK warship HMS Defender and the British military.</span>
    </Button>
  )
}

export default MainNews;
