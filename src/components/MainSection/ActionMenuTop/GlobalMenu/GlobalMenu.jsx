import React from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';

import g1 from './GlobalMenu.module.css';

const GlobalMenu = (props) => {
  return (
    <IconButton edge="start" color="inherit" >
      <MenuIcon />
    </IconButton>
  )
}

export default GlobalMenu;
